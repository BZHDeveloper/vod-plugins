# vod plugins
plugins (chaînes, émissions, ...) pour vod/replay

pré-requis :
- [meson](https://github.com/mesonbuild/meson)
- [ninja](https://github.com/ninja-build/ninja)
- [vala-compile-resources](https://github.com/BZHDeveloper/vala-compile-resources)
- [GXml](https://git.gnome.org/browse/gxml/)
- [GJson](https://github.com/BZHDeveloper/vul)
- [Soup](https://git.gnome.org/browse/libsoup/)

construction :
```
cd vod-plugins
mkdir build && cd build
meson .. --libdir=/dossier/vers/vod-1.0
ninja
sudo ninja install
```
