public class CNewsShow : Vod.Show {
	string uri;
	
	public CNewsShow (GXml.DomElement element) {
		this.uri = element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
		this.name = element.get_elements_by_tag_name ("h3").item (0).node_value.strip();
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri (this.uri, GXml.GHtmlDocument.default_options);
		string image = document.get_elements_by_class_name ("list-grid").item (0)
			.get_elements_by_class_name ("item").item (0).get_elements_by_tag_name ("img").item (0)
			.get_attribute ("src");
		var video = Vod.Streamlink.open (this.uri);
		var icon = new FileIcon (File.new_for_uri (image));
		video.thumbnail = icon;
		videos.add (video);
		foreach (var element in document.get_elements_by_class_name ("list-grid").item (0).get_elements_by_class_name ("item")) {
			var link = element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
			video = Vod.Streamlink.open (link);
			video.thumbnail = icon;
			videos.add (video);
		}
	}
}

public class CNews : Vod.Channel {
	public override string name {
		owned get {
			return "CNEWS";
		}
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri ("http://www.cnews.fr/emissions", GXml.GHtmlDocument.default_options);
		foreach (var element in document.get_elements_by_class_name ("list-grid").item (0).get_elements_by_tag_name ("article"))
			shows.add (new CNewsShow (element));
	}
}
