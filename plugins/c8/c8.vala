public class C8Video : Vod.Video {
	GJson.Object object;
	
	public C8Video (GJson.Node node) {
		this.title = node["title"].as_string();
		uint8[] data;
		File.new_for_uri ("https://secure-service.canal-plus.com/video/rest/getVideos/cplus/%s?format=json".printf (node["contentID"].as_string())).load_contents (null, out data, null);
		this.object = GJson.Object.parse ((string)data);
		this.description = object["INFOS"]["DESCRIPTION"].as_string();
		var ds = object["INFOS"]["PUBLICATION"]["DATE"].as_string();
		var hs = object["INFOS"]["PUBLICATION"]["HEURE"].as_string();
		int y = 0, m = 0, d = 0, h = 0, min = 0, s = 0;
		ds.scanf ("%d/%d/%d", &d, &m, &y);
		hs.scanf ("%d:%d:%d", &h, &min, &s);
		this.release_date = new DateTime.local (y, m, d, h, min, (double)s);
		var url = object["MEDIA"]["IMAGES"]["PETIT"].as_string();
		this.thumbnail = new FileIcon (File.new_for_uri (url));
	}
	
	public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
		object["MEDIA"]["VIDEOS"].as_object().foreach (prop => {
			var entry = new Vod.VideoEntry();
			entry.url = prop.node.as_string();
			entry.quality = prop.name;
			var extension = entry.url.substring (entry.url.last_index_of ("."));
			if (extension == ".mp4") {
				entry.mime_type = "video/mp4";
				entry.url += "?secret=pqzerjlsmdkjfoiuerhsdlfknaes";
				entries.add (entry);
			}
			else if (extension == ".m3u8") {
				var m3u = new Vod.M3u8 (prop.node.as_string());
				foreach (var e in m3u.entries)
					entries.add (new Vod.M3u8VideoEntry (e));
			}
			else
				return true;
			
			return true;
		});
	}
}

public class C8Show : Vod.Show {
	string uri;
	
	public C8Show (GJson.Node node) {
		this.name = node["onClick"]["displayName"].as_string();
		this.uri = node["onClick"]["URLPage"].as_string();
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		uint8[] data;
		File.new_for_uri (this.uri).load_contents (null, out data, null);
		var object = GJson.Object.parse ((string)data);
		object["strates"][0]["contents"].foreach (content => {
			videos.add (new C8Video (content));
			return true;
		});
	}
}

public abstract class C8Group : Vod.Channel {
	
	public override bool get_live (out Vod.Video live_video) {
		return false;
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		uint8[] data;
		File.new_for_uri ("https://www.mycanal.fr/chaines/%s".printf (this.name.down())).load_contents (null, out data, null);
		string json = ((string)data).split ("window.__data=")[1];
		json = json.substring (0, 1 + json.index_of ("};"));
		var object = GJson.Object.parse (json);
		var list = new Gee.ArrayList<string>();
		object["landing"]["strates"].foreach (strate => {
			strate["contents"].foreach (content => {
				if (content["type"].as_string() == "landing" && !list.contains (content["contentID"].as_string())) {
					list.add (content["contentID"].as_string());
					shows.add (new C8Show (content));
				}
				return true;
			});
			return true;
		});
	}

}

public class CHuit : C8Group {
	public override string name {
		owned get {
			return "C8";
		}
	}
}

public class CStar : C8Group {
	public override string name {
		owned get {
			return "CSTAR";
		}
	}
}

public void plugin_init (Vod.Plugin plugin) {
	plugin.types.add (typeof (CHuit));
	plugin.types.add (typeof (CStar));
}
