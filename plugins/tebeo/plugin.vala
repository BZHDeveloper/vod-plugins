public class TelegrammeVideo : Vod.Video {
	string frame_uri;
	
	public TelegrammeVideo (GXml.DomElement element) {
		var uri = element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
		this.thumbnail = new FileIcon (File.new_for_uri (element.get_elements_by_tag_name ("img").item (0).get_attribute ("src")));
		this.title = element.get_elements_by_tag_name ("p").item (0).node_value.strip();
		var document = new GXml.GHtmlDocument.from_uri (uri, GXml.GHtmlDocument.default_options);
		string date = document.get_elements_by_class_name ("moment-format-day").item (0).node_value.strip().replace (" ", "T");
		this.release_date = Vod.parse_date (date);
		this.description = document.get_element_by_id ("cadre_player").get_elements_by_class_name ("description").item (0).node_value.strip();
		this.frame_uri = document.get_element_by_id ("player").get_attribute ("src");
	}
	
	public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
		uint8[] data;
		File.new_for_uri (this.frame_uri).load_contents (null, out data, null);
		string html = (string)data;
		var entry = new Vod.VideoEntry();
		entry.url = html.split ("source: \"")[1].split ("\"")[0];
		entry.quality = "sd";
		entry.mime_type = "video/mp4";
		entries.add (entry);
	}
}

public class TelegrammeShow : Vod.Show {
	string uri;
	
	public TelegrammeShow (GXml.DomElement element) {
		this.uri = element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
		this.name = element.get_elements_by_tag_name ("p").item (0).node_value.strip();
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri (this.uri, GXml.GHtmlDocument.default_options);
		var main = document.get_element_by_id ("items_emissions");
		for (var i = 0; i < main.children.length; i++) {
			if (i == 21)
				break;
			videos.add (new TelegrammeVideo (main.children.item (i)));
		}
	}
}

public abstract class Telegramme : Vod.Channel {
	public class LiveVideo : Vod.Video {
		public LiveVideo (string id) {
			GLib.Object (id : id);
		}
		
		public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
			var m3u = new Vod.M3u8 ("http://live2.awedia.com/hls/%s.m3u8".printf (id));
			foreach (var entry in m3u.entries)
				entries.add (new Vod.M3u8VideoEntry (entry));
		}
		
		public string id { get; construct; }
	}
	
	public abstract string id { owned get; }
	
	public override bool get_live (out Vod.Video live_video) {
		live_video = new LiveVideo (this.id);
		return true;
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri ("http://www.%s.bzh/emissions".printf (id), GXml.GHtmlDocument.default_options);
		foreach (var element in document.get_element_by_id ("items_emissions").get_elements_by_class_name ("emission"))
			shows.add (new TelegrammeShow (element));
	}
}

public class Tebeo : Telegramme {
	public override string name {
		owned get {
			return "Tébéo";
		}
	}
	
	public override string id {
		owned get {
			return "tebeo";
		}
	}
}

public class Tebesud : Telegramme {
	public override string name {
		owned get {
			return "Tébésud";
		}
	}
	
	public override string id {
		owned get {
			return "tebesud";
		}
	}
}

static bool plugin_init (Vod.Plugin plugin) {
	plugin.types.add (typeof (Tebeo));
	plugin.types.add (typeof (Tebesud));
	return true;
}
