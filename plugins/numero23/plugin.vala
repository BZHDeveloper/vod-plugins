public class Numero23Show : Vod.Show {
	Gee.ArrayList<GXml.DomElement> dlist;
	
	public Numero23Show (string show_name, Gee.Collection<GXml.DomElement> collection) {
		this.dlist = new Gee.ArrayList<GXml.DomElement>();
		this.dlist.add_all (collection);
		this.name = show_name;
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		foreach (var element in this.dlist) {
			string dm_id = element.get_elements_by_class_name ("player").item (0).get_attribute ("data-id-video");
			string img = element.get_elements_by_class_name ("figure").item (0).get_elements_by_tag_name ("img").item (0)
				.get_attribute ("src");
			var dm = Vod.Streamlink.open ("http://www.dailymotion.com/embed/video/" + dm_id);
			dm.thumbnail = new FileIcon (File.new_for_uri (img));
			videos.add (dm);
		}
	}
}

public class Numero23 : Vod.Channel {
	public override string name {
		owned get {
			return "Numéro 23";
		}
	}
	
	public override bool get_live (out Vod.Video live_video) {
		live_video = Vod.Streamlink.open ("http://www.dailymotion.com/embed/video/k2ubNS4O075Jnc4vLxE");
		return true;
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var emap = new Gee.HashMultiMap<string, GXml.DomElement>();
		for (var i = 0; i < 9; i++) {
			var document = new GXml.GHtmlDocument.from_uri ("http://www.numero23.fr/replay/?paged=%d".printf (i), GXml.GHtmlDocument.default_options);
			foreach (var element in document.get_elements_by_class_name ("video")) {
				string show_name = element.get_elements_by_class_name ("figcaption").item (0)
					.get_elements_by_tag_name ("h3").item (0).node_value.strip();
				emap[show_name] = element;
			}
		}
		foreach (var key in emap.get_keys())
			shows.add (new Numero23Show (key, emap[key]));
	}
}

static bool plugin_init (Vod.Plugin plugin) {
	plugin.types.add (typeof (Numero23));
	return true;
}
