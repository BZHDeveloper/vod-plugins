public class TF1Video : Vod.Video {
	string uri;
	
	public TF1Video (GXml.DomElement element) {
		this.uri = element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
		this.title = element.get_elements_by_class_name ("description").item (0).get_elements_by_class_name ("title").item (0).node_value.strip();
		var source = element.get_elements_by_tag_name ("source").item (0);
		string thumb = "";
		if (source.get_attribute ("data-srcset") != null)
			thumb = "https:" + source.get_attribute ("data-srcset").split (" ")[0].split (",")[0];
		else
			thumb = "https:" + source.get_attribute ("srcset").split (" ")[0].split (",")[0];
		this.thumbnail = new FileIcon (File.new_for_uri (thumb));
		if (element.get_elements_by_class_name ("description").item (0).get_elements_by_class_name ("stitle").length > 0)
			this.description = element.get_elements_by_class_name ("description").item (0).get_elements_by_class_name ("stitle").item (0).node_value.strip();
		var dt = element.get_elements_by_class_name ("description").item (0).get_elements_by_class_name ("momentDate").item (0).get_attribute ("data-date");
		this.release_date = Vod.parse_date (dt);
	}
	
	public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri (this.uri, GXml.GHtmlDocument.default_options);
		var m3u = new Vod.M3u8 ("http://www.wat.tv/get/iphone/%s.m3u8".printf (document.get_element_by_id ("zonePlayer").get_attribute ("data-watid")));
		foreach (var entry in m3u.entries)
			entries.add (new Vod.M3u8VideoEntry (entry));
	}
}

public class TF1Show : Vod.Show {
	public TF1Show (GXml.DomElement element) {
		this.uri = element.get_elements_by_tag_name ("a").item (0).get_attribute ("href") + "/videos";
		this.name = element.get_elements_by_class_name ("description").item (0)
			.get_elements_by_class_name ("text").item (0).node_value.strip();
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri (this.uri, GXml.GHtmlDocument.default_options);
		if (document.get_elements_by_class_name ("content").length == 0)
			return;
		foreach (var element in document.get_elements_by_class_name ("content").item (0)
			.get_elements_by_class_name ("grid").item (0).get_elements_by_class_name ("key-list-videos"))
				videos.add (new TF1Video (element));
	}
	
	public string uri { get; private set; }
}

public abstract class TF1Group : Vod.Channel {
	public override bool get_live (out Vod.Video live_video) {
		try {
			live_video = Vod.Streamlink.open ("tf1.fr/%s/direct".printf (name.down().replace (" ", "-")));
			return true;
		}
		catch {}
		return false;
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri ("https://www.tf1.fr/%s/programmes-tv".printf (name.down().replace (" ", "-")), GXml.GHtmlDocument.default_options);
		foreach (var element in document.get_element_by_id ("js_filter_el_container").get_elements_by_class_name ("js_filter_el"))
			shows.add (new TF1Show (element));
	}
}

public class TF1 : TF1Group {
	public override string name {
		owned get {
			return "TF1";
		}
	}
}

public class TMC : TF1Group {
	public override string name {
		owned get {
			return "TMC";
		}
	}
}

public class TFX : TF1Group {
	public override string name {
		owned get {
			return "TFX";
		}
	}
}

public class TF1SF : TF1Group {
	public override string name {
		owned get {
			return "TF1 Series Films";
		}
	}
}

static bool plugin_init (Vod.Plugin plugin) {
	plugin.types.add (typeof (TF1));
	plugin.types.add (typeof (TMC));
	plugin.types.add (typeof (TFX));
	plugin.types.add (typeof (TF1SF));
	plugin.types.add (typeof (LCI));
	return true;
}
