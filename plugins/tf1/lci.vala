public class LCIVideo : Vod.Video {
	string uri;
	
	public LCIVideo (GXml.DomElement element) {
		this.title = element.get_elements_by_class_name ("title-small").item (0).node_value.strip();
		var source = element.get_elements_by_tag_name ("source").item (0);
		string thumb = "";
		if (source.get_attribute ("data-srcset") != null)
			thumb = source.get_attribute ("data-srcset").split (" ")[0].split (",")[0];
		else
			thumb = source.get_attribute ("srcset").split (" ")[0].split (",")[0];
		this.thumbnail = new FileIcon (File.new_for_uri (thumb));
		this.uri = "http://www.lci.fr" + element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
	}
	
	public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
		uint8[] data;
		File.new_for_uri (this.uri).load_contents (null, out data, null);
		string html = (string)data;
		if (!(html.split ("data-watid=\"").length > 1))
			return;
		var m3u = new Vod.M3u8 ("http://www.wat.tv/get/iphone/%s.m3u8".printf (html.split ("data-watid=\"")[1].split ("\"")[0]));
		foreach (var entry in m3u.entries)
			entries.add (new Vod.M3u8VideoEntry (entry));
	}
}

public class LCIShow : Vod.Show {
	string uri;
	
	public LCIShow (GXml.DomElement element) {
		this.uri = "http://www.lci.fr" + element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
		this.name = element.get_elements_by_tag_name ("h2").item (0).node_value.strip();
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri (this.uri, GXml.GHtmlDocument.default_options);
		foreach (var element in document.get_elements_by_class_name ("topic-emission-extract-block").item (0)
			.get_elements_by_class_name ("medium-3col-article-block-article")) {
				videos.add (new LCIVideo (element));
			}
	}
}

public class LCI : Vod.Channel {
	public override string name {
		owned get {
			return "LCI";
		}
	}
	
	public override bool get_live (out Vod.Video live_video) {
		live_video = Vod.Streamlink.open ("tf1.fr/lci/direct");
		return true;
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri ("http://www.lci.fr/emissions", GXml.GHtmlDocument.default_options);
		foreach (var element in document.get_elements_by_property_value ("data-channel", "LCI"))
			shows.add (new LCIShow (element));
	}
}
