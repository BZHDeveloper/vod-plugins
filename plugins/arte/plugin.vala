public class ArteVideo : Vod.Video {
	GJson.Object vsr;
	
	public ArteVideo (GJson.Node node) throws GLib.Error {
		if (node["subtitle"].node_type == GJson.NodeType.STRING)
			this.title = node["subtitle"].as_string();
		else
			this.title = node["title"].as_string();
		string thumb = null;
		node["images"].foreach (img => {
			if (img["format"].as_string() == "landscape")
				thumb = img["url"].as_string();
			return true;
		});
		if (thumb == null)
			thumb = node["images"][0]["url"].as_string();
		this.thumbnail = new FileIcon (File.new_for_uri (thumb));
		this.release_date = Vod.parse_date (node["beginsAt"].as_string());
		var stream = File.new_for_uri ("https://api.arte.tv/api/player/v1/config/fr/" + node["programId"].as_string()).read();
		var reader = new GJson.TextReader.from_stream (stream);
		var object = GJson.Object.load (reader);
		this.description = object["videoJsonPlayer"]["V7T"].as_string();
		this.vsr = object["videoJsonPlayer"]["VSR"].as_object();
	}
	
	public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
		vsr.foreach (prop => {
			var entry = new Vod.VideoEntry();
			entry.url = prop.node["url"].as_string();
			entry.quality = prop.node["quality"].as_string();
			entry.mime_type = prop.node["mimeType"].as_string();
			if (prop.node["url"].as_string().has_suffix (".m3u8")) {
				var m3u = new Vod.M3u8 (prop.node["url"].as_string());
				foreach (var e in m3u.entries)
					entries.add (new Vod.M3u8VideoEntry (e));
			}
			else
				entries.add (entry);
			return true;
		});
	}
}

public class ArteShow : Vod.Show {
	Gee.ArrayList<GJson.Node> list;
	
	public ArteShow (string name, Gee.Collection<GJson.Node> nodes) {
		this.name = name;
		this.list = new Gee.ArrayList<GJson.Node>();
		this.list.add_all (nodes);
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		foreach (var node in this.list) {
			var video = new ArteVideo (node);
			videos.add (video);
		}
	}
}

public class Arte : Vod.Channel {
	public override string name {
		owned get {
			return "arte";
		}
	}
	
	public override bool get_live (out Vod.Video live_video) {
		live_video = Vod.Streamlink.open ("https://www.arte.tv/fr/direct/");
		return true;
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var map = new Gee.HashMultiMap<string, GJson.Node>();
		var date = new DateTime.now_local();
		for (var i = 0; i < 8; i++) {
			var stream = File.new_for_uri ("https://www.arte.tv/guide/api/api/pages/fr/web/tv_guide?day=" + date.format ("%y-%m-%d")).read();
			var reader = new GJson.TextReader.from_stream (stream);
			var object = GJson.Object.load (reader);
			object["zones"].foreach (zone => {
				zone["teasers"].foreach (teaser => {
					map[teaser["title"].as_string()] = teaser;
					return true;
				});
				return true;
			});
			date = date.add (-1 * TimeSpan.DAY);
		}
		map.get_keys().foreach (key => {
			shows.add (new ArteShow (key, map[key]));
			return true;
		});
	}
}

static bool plugin_init (Vod.Plugin plugin) {
	plugin.types.add (typeof (Arte));
	return true;
}
