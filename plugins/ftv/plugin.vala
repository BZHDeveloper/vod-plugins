public class FranceTeleVideo : Vod.Video {
	string video_id;
	string m3u_url;
	
	public FranceTeleVideo (GXml.DomElement element) {
		this.uri = "https:" + element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
		this.video_id = element.get_elements_by_class_name ("card-link").item (0).get_attribute ("data-video");
		var thumb = "https:" + element.get_elements_by_tag_name ("img").item (0).get_attribute ("data-src");
		this.thumbnail = new FileIcon (File.new_for_uri (thumb));
		this.title = element.get_elements_by_class_name ("card-content").item (0).get_elements_by_tag_name ("p").item (0).node_value.strip();
	}
	
	public void update() {
		uint8[] data;
		File.new_for_uri ("https://sivideo.webservices.francetelevisions.fr/tools/getInfosOeuvre/v2/?idDiffusion=" + video_id).load_contents (null, out data, null);
		var object = GJson.Object.parse ((string)data);
		this.description = object["description"].as_string();
		this.release_date = new DateTime.from_unix_local (object["diffusion"]["timestamp"].as_integer());
		object["videos"].foreach (vid => {
			if (vid["format"].as_string() == "m3u8-download")
				this.m3u_url = vid["url"].as_string();
			return true;
		});
	}
	
	public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
		var m3u = new Vod.M3u8 (this.m3u_url);
		foreach (var entry in m3u.entries)
			entries.add (new Vod.M3u8VideoEntry (entry));
	}
	
	public string uri { get; private set; }
}

public class FranceTeleShow : Vod.Show {
	Gee.ArrayList<FranceTeleVideo> video_list;
	
	public FranceTeleShow (string show_name, Gee.Collection<FranceTeleVideo> videos) {
		this.video_list = new Gee.ArrayList<FranceTeleVideo>();
		this.video_list.add_all (videos);
		this.name = show_name;
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		foreach (var vid in this.video_list) {
			vid.update();
			videos.add (vid);
		}
	}
}

public abstract class FranceTele : Vod.Channel {
	public class LiveVideo : Vod.Video {
		string live_id;
		
		public LiveVideo (string live_id) {
			this.live_id = live_id;
		}
		
		public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
			uint8[] data;
			File.new_for_uri ("http://hdfauth.francetv.fr/esi/TA?format=json&url=http://live.francetv.fr/simulcast/%s/hls_v1/index.m3u8".printf (live_id)).load_contents (null, out data, null);
			var object = GJson.Object.parse ((string)data);
			var m3u_url = object["url"].as_string();
			var e = new Vod.VideoEntry();
			e.url = m3u_url;
			e.quality = "adaptive";
			e.mime_type = "application/vnd.apple.mpegurl";
			entries.add (e);
			var m3u = new Vod.M3u8 (m3u_url);
			foreach (var entry in m3u.entries)
				entries.add (new Vod.M3u8VideoEntry (entry));
		}
	}
	
	public abstract string id { owned get; }
	
	public override bool get_live (out Vod.Video live_video) {
		live_video = new LiveVideo (this.id);
		return true;
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var map = new Gee.HashMultiMap<string, FranceTeleVideo>();
		for (var i = 0; i < 15; i++) {
			var document = new GXml.GHtmlDocument.from_uri ("https://www.france.tv/%s/contents?page=%d".printf (id.down().replace ("_", "-"), i), GXml.GHtmlDocument.default_options);
			foreach (var element in document.get_elements_by_class_name ("card-small")) {
				string show_name = element.get_elements_by_class_name ("card-content").item (0)
					.get_elements_by_tag_name ("h3").item (0).node_value.strip();
				map[show_name] = new FranceTeleVideo (element);
			}
		}
		foreach (string key in map.get_keys()) {
			shows.add (new FranceTeleShow (key, map[key]));
		}
	}
}

public class France2 : FranceTele {
		public override string id {
		owned get {
			return "France_2";
		}
	}
	
	public override string name {
		owned get {
			return "France 2";
		}
	}
}

public class France3 : FranceTele {
		public override string id {
		owned get {
			return "France_3";
		}
	}
	
	public override string name {
		owned get {
			return "France 3";
		}
	}
}

public class France4 : FranceTele {
		public override string id {
		owned get {
			return "France_4";
		}
	}
	
	public override string name {
		owned get {
			return "France 4";
		}
	}
}

public class France5 : FranceTele {
		public override string id {
		owned get {
			return "France_5";
		}
	}
	
	public override string name {
		owned get {
			return "France 5";
		}
	}
}

public class FranceO : FranceTele {
	public override string id {
		owned get {
			return "France_O";
		}
	}
	
	public override string name {
		owned get {
			return "France Ô";
		}
	}
}

static bool plugin_init (Vod.Plugin plugin) {
	plugin.types.add (typeof (France2));
	plugin.types.add (typeof (France3));
	plugin.types.add (typeof (France4));
	plugin.types.add (typeof (France5));
	plugin.types.add (typeof (FranceO));
	return true;
}
