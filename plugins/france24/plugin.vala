public class France24Video : Vod.Video {
	GXml.GHtmlDocument document;
	
	public France24Video (string id) {
		this.document = new GXml.GHtmlDocument.from_uri ("https://www.youpak.com/watch?v=" + id, GXml.GHtmlDocument.default_options);
		this.title = this.document.get_elements_by_class_name ("video-title").item (0).text_content;
		this.description = this.document.get_element_by_id ("about").get_elements_by_tag_name ("p").item (0).text_content;
		this.thumbnail = new FileIcon (File.new_for_uri ("https://ytimg.googleusercontent.com/vi/" + id + "/mqdefault.jpg"));
	}
	
	public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
		foreach (var btn in document.get_element_by_id ("downloadtoggle").get_elements_by_class_name ("btn-block")) {
			var entry = new Vod.VideoEntry();
			entry.url = btn.get_attribute ("href");
			entry.quality = btn.text_content.strip();
			entry.mime_type = btn.get_attribute ("href").split ("mime=")[1].split ("&")[0];
			entries.add (entry);
		}
	}
}

public class France24Show : Vod.Show {
	string link;
	
	public France24Show (GXml.DomElement node) {
		var h3 = node.get_elements_by_class_name ("copy").item (0).get_elements_by_tag_name ("h3").item (0);
		this.name = h3.text_content;
		this.description = h3.next_element_sibling.text_content;
		this.link = "http://www.france24.com" + node.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri (this.link, GXml.GHtmlDocument.default_options);
		var f = document.get_elements_by_class_name ("f-expand-items");
		if (f.length == 0)
			return;
		var url = "http://www.france24.com" + f.item (0).get_elements_by_tag_name ("a").item (0).get_attribute ("href");
		document = new GXml.GHtmlDocument.from_uri (url, GXml.GHtmlDocument.default_options);
		foreach (var item in document.get_elements_by_class_name ("news-item")) {
			string l = "http://www.france24.com" + node.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
			var doc = new GXml.GHtmlDocument.from_uri (l, GXml.GHtmlDocument.default_options);
			string date = "";
			foreach (var meta in doc.get_elements_by_tag_name ("meta"))
				if (meta.get_attribute ("property") == "article:published_time")
					date = meta.get_attribute ("content");
			var dt = Vod.parse_date (date);
			string id = doc.get_elements_by_class_name ("youtube-container").item (0).get_attribute ("data-ytplayer-id");
			var yt = new France24Video (id);
			yt.release_date = yt;
			videos.add (yt);
		}
	}
}

public class France24 : Vod.Channel {
	public override string name {
		owned get {
			return "France 24";
		}
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri ("http://www.france24.com/fr/emissions/", GXml.GHtmlDocument.default_options);
		foreach (var item in document.get_elements_by_class_name ("detailed-emission-item"))
			shows.add (new France24Show (item));
	}
}

[ModuleInit]
static bool plugin_init (TypeModule module) {
	Vod.Plugin plugin = module as Vod.Plugin;
	plugin.types.add (typeof (France24));
	return true;
}
