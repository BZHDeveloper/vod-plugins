public class BFMShow : Vod.Show {
	string uri;
	
	public BFMShow (GXml.DomElement element) {
		this.name = element.get_elements_by_tag_name ("img").item (0).get_attribute ("alt");
		this.uri = element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
	}
	
	public bool valid { get; private set; }
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri (this.uri, GXml.GHtmlDocument.default_options);
		foreach (var element in document.get_elements_by_tag_name ("article")) {
			string link = element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
			var video = Vod.Streamlink.open (link);
			var doc = new GXml.GHtmlDocument.from_uri (link, GXml.GHtmlDocument.default_options);
			var thumb = element.get_elements_by_class_name ("article-img").item (0).get_attribute ("data-original");
			video.thumbnail = new FileIcon (File.new_for_uri (thumb));
			video.title = element.get_elements_by_class_name ("art-body").item (0).node_value.strip();
			video.description = doc.get_elements_by_property_value ("itemprop", "description").item (0).get_attribute ("content");
			video.release_date = Vod.parse_date (doc.get_elements_by_property_value ("itemprop", "uploadDate").item (0).get_attribute ("content"));
			videos.add (video);
		}
	}
}

public abstract class BFM : Vod.Channel {
	public abstract string id { owned get; }
	
	
	public override bool get_live (out Vod.Video live_video) {
		live_video = Vod.Streamlink.open ("http://%s/mediaplayer/live-video/".printf (id));
		return true;
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {	
		var document = new GXml.GHtmlDocument.from_uri ("http://%s/mediaplayer/replay/".printf (this.id), GXml.GHtmlDocument.default_options);
		var parent = document.get_elements_by_class_name ("art-bloc").item (0).parent_element;
		foreach (var element in parent.get_elements_by_class_name ("art-bloc"))
			shows.add (new BFMShow (element));
	}
}

public class BFMTV : BFM {
	public override string id {
		owned get {
			return "www.bfmtv.com";
		}
	}
	
	public override string name {
		owned get {
			return "BFM TV";
		}
	}
}

public class BFMBusiness : BFM {
	public override string id {
		owned get {
			return "bfmbusiness.bfmtv.com";
		}
	}
	
	public override string name {
		owned get {
			return "BFM Business";
		}
	}
}

public class TV01Net : BFM {
	public override string id {
		owned get {
			return "www.01net.com";
		}
	}
	
	public override string name {
		owned get {
			return "01net TV";
		}
	}
}

static bool plugin_init (Vod.Plugin plugin) {
	plugin.types.add (typeof (BFMTV));
	plugin.types.add (typeof (BFMBusiness));
	plugin.types.add (typeof (TV01Net));
	plugin.types.add (typeof (RMCDecouverte));
	return true;
}
