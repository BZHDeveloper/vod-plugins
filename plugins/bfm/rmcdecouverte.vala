public class RMCDecouverteShow : Vod.Show {
	Gee.ArrayList<GXml.DomElement> list;
	
	public RMCDecouverteShow (string show_name, Gee.Collection<GXml.DomElement> children) {
		this.name = show_name;
		this.list = new Gee.ArrayList<GXml.DomElement>();
		this.list.add_all (children);
	}
	
	public bool get_video (GXml.DomElement element, out Vod.Streamlink video) throws GLib.Error {
		string link = "http://rmcdecouverte.bfmtv.com/mediaplayer-replay/?id=" + element.get_elements_by_tag_name ("a").item (0).get_attribute ("href").split ("id=")[1].split ("&")[0];
		video = Vod.Streamlink.open (link);
		var img = element.get_elements_by_tag_name ("img").item (0);
		video.title = img.get_attribute ("title");
		video.thumbnail = new FileIcon (File.new_for_uri (img.get_attribute ("data-original")));
		foreach (var node in element.get_elements_by_tag_name ("article").item (0).child_nodes)
			if (node is GXml.Comment) {
				var comment = node as GXml.Comment;
				if (comment.str.contains ("class=")) {
					string date = comment.str.split ("</i>")[1].split ("</div>")[0].strip();
					int y = 0, m = 0, d = 0, h = 0, min = 0, s = 0;
					date.scanf ("%d/%d/%d %d:%d:%d", &d, &m, &y, &h, &min, &s);
					video.release_date = new DateTime.local (y, m, d, h, min, (double)s);
				}
			}
		var document = new GXml.GHtmlDocument.from_uri (link, GXml.GHtmlDocument.default_options);
		video.description = document.get_elements_by_property_value ("itemprop", "description").item (0).node_value.strip();
		return document.get_elements_by_class_name ("next-player").length > 0;
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		foreach (var element in this.list) {
			Vod.Streamlink sl = null;
			if (get_video (element, out sl))
				videos.add (sl);
		}
	}
}

public class RMCDecouverte : Vod.Channel {
	public override string name {
		owned get {
			return "RMC Découverte";
		}
	}
	
	public override bool get_live (out Vod.Video live_video) {
		live_video = Vod.Streamlink.open ("http://rmcdecouverte.bfmtv.com/mediaplayer-direct/");
		return true;
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var emap = new Gee.HashMultiMap<string, GXml.DomElement>();
		var document = new GXml.GHtmlDocument.from_uri ("http://rmcdecouverte.bfmtv.com/mediaplayer-replay/", GXml.GHtmlDocument.default_options);
		foreach (var element in document.get_element_by_id ("fil_replay").get_elements_by_class_name ("art-bloc")) {
			var img = element.get_elements_by_tag_name ("img").item (0);
			string title = img.get_attribute ("title");
			string vtitle = element.get_elements_by_class_name ("art-body").item (0)
				.get_elements_by_tag_name ("h2").item (0).node_value.strip();
			string show_name = vtitle.split (title)[0].strip();
			if (show_name.length == 0)
				show_name = title;
			else
				show_name = show_name.substring (0, show_name.last_index_of (":")).strip();
			emap[show_name] = element;
		}
		foreach (var key in emap.get_keys())
			shows.add (new RMCDecouverteShow (key, emap[key]));
	}
}
