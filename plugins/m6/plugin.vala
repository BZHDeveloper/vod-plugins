public class M6Video : Vod.Video {
	GJson.Node assets;
	
	public M6Video (GJson.Node node) {
		string image = "";
		node["images"].foreach (img => {
			if (img["role"].as_string() == "vignette")
				image = img["external_key"].as_string();
			return true;
		});
		if (image == "")
			image = node["images"][0]["external_key"].as_string();
		this.thumbnail = new FileIcon (File.new_for_uri ("https://images.6play.fr/v1/images/%s/raw".printf (image)));
		this.description = node["description"].as_string();
		this.title = node["title"].as_string();
		this.release_date = Vod.parse_date (node["clips"][0]["product"]["last_diffusion"].as_string().replace (" ", "T"));
		this.assets = node["clips"][0]["assets"];
	}
	
	public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
		this.assets.foreach (asset => {
			var entry = new Vod.VideoEntry();
			entry.url = asset["full_physical_path"].as_string();
			entry.quality = asset["video_quality"].as_string();
			entry.mime_type = "video/" + asset["video_container"].as_string();
			if (asset["video_container"].as_string() == "ism") {
				var m3u = new Vod.M3u8 (asset["full_physical_path"].as_string());
				foreach (var e in m3u.entries)
					entries.add (new Vod.M3u8VideoEntry (e));
			}
			else if (asset["protocol"].as_string() != "primetime")
				entries.add (entry);
			return true;
		});
	}
}

public class M6Show : Vod.Show {
	int64 id;
	
	public M6Show (GJson.Node node) {
		this.name = node["title"].as_string();
		string logo = "";
		node["images"].foreach (img => {
			if (img["role"].as_string() == "logo")
				logo = img["external_key"].as_string();
			return true;
		});
		if (logo == "")
			logo = node["images"][0]["external_key"].as_string();
		this.valid = logo.strip().length > 0;
		if (this.valid)
			this.id = node["id"].as_integer();
	}
	
	public bool valid { get; private set; }
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		uint8[] data;
		File.new_for_uri ("https://pc.middleware.6play.fr/6play/v2/platforms/m6group_web/services/6play/programs/%lld/videos?csa=6&with=clips,freemiumpacks&type=vi,vc,playlist&limit=50&offset=0".printf (id)).load_contents (null, out data, null);
		var array = GJson.Array.parse ((string)data);
		array.foreach (node => {
			videos.add (new M6Video (node));
			return true;
		});
	}
}

public abstract class M6Group : Vod.Channel {
	public string id { get; construct; }
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		uint8[] data;
		File.new_for_uri ("https://pc.middleware.6play.fr/6play/v2/platforms/m6group_web/services/%s/folders?limit=100&offset=0".printf (id)).load_contents (null, out data, null);
		var array = GJson.Array.parse ((string)data);
		array.foreach (node => {
			int64 cat = node["id"].as_integer();
			File.new_for_uri ("https://pc.middleware.6play.fr/6play/v2/platforms/m6group_web/services/6play/folders/%lld/programs?limit=100&offset=0&csa=6&with=parentcontext".printf (cat)).load_contents (null, out data, null);
			var carray = GJson.Array.parse ((string)data);
			carray.foreach (child => {
				var show = new M6Show (child);
				if (show.valid)
					shows.add (show);
				return true;
			});
			return true;
		});
	}
}

public class MSix : M6Group {
	public override string name {
		owned get {
			return "M6";
		}
	}
}

public class WNeuf : M6Group {
	public override string name {
		owned get {
			return "W9";
		}
	}
}

public class Sixter : M6Group {
	public override string name {
		owned get {
			return "6ter";
		}
	}
}

static bool plugin_init (Vod.Plugin plugin) {
	plugin.types.add (typeof (MSix));
	plugin.types.add (typeof (WNeuf));
	plugin.types.add (typeof (Sixter));
	return true;
}
