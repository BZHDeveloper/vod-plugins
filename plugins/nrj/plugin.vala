public class NRJVideo : Vod.Video {
	string uri;
	
	public NRJVideo (GXml.DomElement element) {
		this.title = element.get_elements_by_class_name ("thumbnailReplay-title").item (0).node_value.strip();
		var thumb = element.get_elements_by_class_name ("thumbnailReplay-visual").item (0)
			.get_elements_by_tag_name ("img").item (0).get_attribute ("src");
		this.thumbnail = new FileIcon (File.new_for_uri (thumb));
		if (element.get_elements_by_tag_name ("time").length > 0) {
			string date = element.get_elements_by_tag_name ("time").item (0).get_attribute ("datetime") + "T00:00:00";
			this.release_date = Vod.parse_date (date);
		}
		else
			this.release_date = new DateTime.now_local();
		this.uri = element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
		if (!this.uri.has_prefix ("http://") && !this.uri.has_prefix ("https://"))
			this.uri = "http://www.nrj-play.fr" + this.uri;
	}
	
	public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri (this.uri, GXml.GHtmlDocument.default_options);
		this.description = document.get_elements_by_class_name ("nrjVideo-description").item (0)
			.get_elements_by_tag_name ("p").item (0).node_value.strip();
		var entry = new Vod.VideoEntry();
		string content_url = "";
		foreach (var meta in document.get_elements_by_tag_name ("meta"))
			if (meta.get_attribute ("itemprop") == "contentUrl")
				content_url = meta.get_attribute ("content");
		entry.url = content_url;
		entry.quality = "hd";
		entry.mime_type = "video/mp4";
		entries.add (entry);
	}
}

public class NRJShow : Vod.Show {
	string uri;
	
	public NRJShow (GXml.DomElement element) {
		this.uri = element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
		if (!this.uri.has_prefix ("http://") || !this.uri.has_prefix ("https://"))
			this.uri = "http://www.nrj-play.fr" + this.uri;
		this.name = element.get_elements_by_class_name ("linkProgram-title").item (0).node_value.strip();
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri (this.uri, GXml.GHtmlDocument.default_options);
		foreach (var element in document.get_elements_by_class_name ("carousel-mea-replay").item (0)
			.get_elements_by_class_name ("thumbnailReplay"))
				videos.add (new NRJVideo (element));
	}
}

public abstract class NRJGroup : Vod.Channel {
	public string id { get; construct; }
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri ("http://www.nrj-play.fr/%s/programmes".printf ((this is NRJ12) ? "nrj12" : "cherie25"), GXml.GHtmlDocument.default_options);
		foreach (var element in document.get_elements_by_class_name ("list-programs").item (0).get_elements_by_class_name ("linkProgram")) {
			if (element.get_elements_by_class_name ("linkProgram-quantity").length > 0)
				shows.add (new NRJShow (element));
		}
	}
}

public class NRJ12 : NRJGroup {
	public override string name {
		owned get {
			return "NRJ 12";
		}
	}
}

public class Cherie25 : NRJGroup {
	public override string name {
		owned get {
			return "Chérie 25";
		}
	}
}

static bool plugin_init (Vod.Plugin plugin) {
	plugin.types.add (typeof (NRJ12));
	plugin.types.add (typeof (Cherie25));
	return true;
}
