public class GameOneVideoEntry : Vod.VideoEntry {
	public override GLib.InputStream get_stream() throws GLib.Error {
		return new RtmpInputStream (this.url);
	}
}

public class GameOneVideo : Vod.Video {
	public GameOneVideo (GXml.DomElement element) {
		this.uri = "http://www.gameone.net" + element.get_elements_by_tag_name ("a").item (0).get_attribute ("href");
		this.thumbnail = new FileIcon (File.new_for_uri (element.get_elements_by_tag_name ("img").item (0).get_attribute ("src")));
		this.title = element.get_elements_by_tag_name ("h3").item (0).node_value.strip();
	}
	
	string uri;
	string mtv_id;
	
	public void update() throws GLib.Error {
		var document = new GXml.GHtmlDocument.from_uri (this.uri, GXml.GHtmlDocument.default_options);
		this.description = document.get_elements_by_class_name ("episode-box").item (0).node_value.strip();
		string date = document.get_elements_by_property_value ("itemprop", "uploadDate").item (0).get_attribute ("content");
		this.release_date = Vod.parse_date (date);
		this.mtv_id = document.get_elements_by_class_name ("mtv-video-player").item (0).get_attribute ("data-mtv-uri");
	}
	
	public override void fill (Gee.List<Vod.VideoEntry> entries) throws GLib.Error {
		var document = new GXml.GDocument.from_uri ("http://intl.mtvnservices.com/mediagen/%s/?device=Other&acceptMethods=fms,hdn1,hds".printf (Uri.escape_string (this.mtv_id)));
		foreach (var rendition in document.get_elements_by_tag_name ("rendition")) {
			var entry = new GameOneVideoEntry();
			entry.url = rendition.get_elements_by_tag_name ("src").item (0).node_value.strip();
			entry.quality = rendition.get_attribute ("bitrate");
			entry.mime_type = "video/mp4";
			entries.add (entry);
		}
	}
}

public class GameOneShow : Vod.Show {
	Gee.ArrayList<GameOneVideo> gov;
	
	public GameOneShow (string show_name, Gee.Collection<GameOneVideo> collection) {
		this.gov = new Gee.ArrayList<GameOneVideo>();
		this.gov.add_all (collection);
		this.name = show_name;
	}
	
	public override void fill (Gee.List<Vod.Video> videos) throws GLib.Error {
		foreach (var video in gov) {
			video.update();
			videos.add (video);
		}
	}
}

public class GameOne : Vod.Channel {
	public override string name {
		owned get {
			return "GameOne";
		}
	}
	
	public override void fill (Gee.List<Vod.Show> shows) throws GLib.Error {
		var smap = new Gee.HashMultiMap<string, GameOneVideo>();
		for (var i = 1; i < 3; i++) {
			var document = new GXml.GHtmlDocument.from_uri ("http://www.gameone.net/dernieres-videos/%d".printf (i), GXml.GHtmlDocument.default_options);
			foreach (var element in document.get_elements_by_class_name ("thumbnail")) {
				var show_name = element.get_elements_by_tag_name ("em").item (0).node_value.strip();
				smap[show_name] = new GameOneVideo (element);
			}
		}
		foreach (var key in smap.get_keys())
			shows.add (new GameOneShow (key, smap[key]));
	}
}

static bool plugin_init (Vod.Plugin plugin) {
	plugin.types.add (typeof (GameOne));
	return true;
}
