[CCode (cheader_filename = "librtmp/rtmp.h")]
namespace LibRtmp {
	[CCode (cname = "RTMP_LNK")]
	public struct Link {
		public int timeout;
	}
	
	[Compact]
	[CCode (cname = "RTMP", cprefix = "RTMP_", free_function = "RTMP_Free")]
	public class Rtmp {
		[CCode (cname = "RTMP_Alloc")]
		public Rtmp();
		public void Init();
		public void Close();
		public bool SetupURL (string url);
		public bool IsConnected();
		public bool Connect (void* data = null);
		public int Read ([CCode (type = "gchar*")] uint8[] data);
		
		public Link Link;
	}
}
