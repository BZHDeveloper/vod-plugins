using LibRtmp;

public class RtmpInputStream : GLib.InputStream {
	Rtmp rtmp;
	
	public RtmpInputStream (string uri) throws GLib.IOError {
		this.rtmp = new Rtmp();
		this.rtmp.Init();
		this.rtmp.Link.timeout = 120;
		if (!this.rtmp.SetupURL (uri))
			throw new IOError.FAILED ("RtmpInputStream : failed to setup '%s' url".printf (uri));
		if (!this.rtmp.Connect())
			throw new IOError.FAILED ("RtmpInputStream : Could not connect to RTMP stream '%s'".printf (uri));
	}
	
	public override bool close (GLib.Cancellable? cancellable = null) throws GLib.IOError {
		if (cancellable != null && cancellable.is_cancelled())
			return false;
		if (this.rtmp.IsConnected())
			this.rtmp.Close();
		else
			return false;
		return true;
	}
	
	public override ssize_t read (uint8[] data, GLib.Cancellable? cancellable = null) throws GLib.IOError {
		if (cancellable != null && cancellable.is_cancelled())
			return 0;
		return this.rtmp.Read (data);
	}
}
